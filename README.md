# GNA is Not Argparse

GNA is an (early state) library allowing to manage declarative
command-line argument configurations.


# Example 

Assume the following definition in `config.json` :

```json
{
    "epochs_nb": {
        "type": "int",
        "default": "5",
        "help": "number of epochs"
    }
}
```

Here is a simple example of script creating and using a `Config` from
this file :

```python
import sys
from gna.config import Config

if __name__ == "__main__":
    config = Config("./test.json")
    print(f"epochs number : {config.epochs_nb}")
```

You can now call your script as follows :

> python3 script.py --epochs-nb 10

You can also use the parameters with their "short form", which is
automatically derived by GNA :

> python3 script.py -en 10


