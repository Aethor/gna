from typing import List, Optional, Callable, Any, Mapping, Tuple
import argparse, json, re, sys


def bool_initializer(arg: str):
    return arg != "false"


default_initializers = {
    "int": int,
    "float": float,
    "str": str,
    "bool": bool_initializer,
}


class Config:
    """"""

    def __init__(
        self,
        definition_file_path: str,
        args: Optional[List[str]] = None,
        custom_initializers: Optional[Mapping[str, Callable[[str], Any]]] = None,
    ):
        if args is None:
            args = sys.argv[1:]

        cf_argument_parser = argparse.ArgumentParser(add_help=False)
        cf_argument_parser.add_argument("-cfp", "--config-file-path", type=str)
        known_args, _ = cf_argument_parser.parse_known_args(args)
        config_file_path: Optional[str] = known_args.config_file_path

        with open(definition_file_path) as definition_file:
            self.definition = json.load(definition_file)

        self.short_calls_str = set()
        self.long_calls_str = set()

        self.parameters = {}
        if not config_file_path is None:
            with open(config_file_path) as config_file:
                config_file_parameters = json.loads(config_file.read())
            for key, value in config_file_parameters.items():
                if not key in self.definition:
                    raise Exception(
                        "Error parsing config file : unknown argument {}".format(key)
                    )
                parameter_definition = self.definition[key]
                initializer = self._get_initializer(
                    parameter_definition["type"], custom_initializers
                )
                if initializer is None:
                    raise Exception(
                        "Error parsing config file : no initializer for type {}".format(
                            parameter_definition["type"]
                        )
                    )
                self.parameters[key] = initializer(value)

        argument_parser = argparse.ArgumentParser()
        for parameter_name, parameter_definition in self.definition.items():
            initializer = self._get_initializer(
                parameter_definition["type"], custom_initializers
            )
            if initializer is None:
                raise Exception(
                    "Error parsing command line arguments : no initializer for type {}".format(
                        parameter_definition["type"]
                    )
                )
            short_call_str, long_call_str = self._get_parameter_call_str(parameter_name)
            self.short_calls_str.add(short_call_str)
            self.long_calls_str.add(long_call_str)
            argument_parser.add_argument(
                short_call_str,
                long_call_str,
                type=initializer,
                nargs="?",
                const="",
                default=parameter_definition.get("default", None),
                help=parameter_definition.get("help", ""),
            )
        parsed_parameters = vars(argument_parser.parse_args())
        for key, value in parsed_parameters.items():
            self.parameters[key] = value

    def _get_parameter_call_str(self, parameter_name: str) -> Tuple[str, str]:
        if parameter_name == "":
            raise Exception(f"Invalid parameter name : {parameter_name}")

        splits = [s for s in parameter_name.split("_") if not s == ""]
        if len(splits) == 0:
            raise Exception(f"Invalid parameter name : {parameter_name}")

        short_call = "-" + "".join(s[0] for s in splits)
        selected_split_idx = len(splits) - 1
        selected_char_idx = [0] * len(splits)
        while short_call in self.short_calls_str:
            if (
                selected_char_idx[selected_split_idx]
                == len(splits[selected_split_idx]) - 1
            ):
                if selected_split_idx == 0:
                    raise Exception(
                        f"Impossible to find unique short parameter call for parameter {parameter_name}"
                    )
                selected_split_idx -= 1
            else:
                selected_char_idx[selected_split_idx] += 1
            short_call = "-" + "".join(s[i] for s, i in zip(splits, selected_char_idx))

        long_call = "--" + re.sub("_", "-", parameter_name)
        if long_call in self.long_calls_str:
            raise Exception(f"Parameter duplication of {parameter_name}")

        return short_call, long_call

    def _get_initializer(
        self,
        type_str: str,
        custom_initializers: Optional[Mapping[str, Callable[[str], Any]]],
    ) -> Optional[Callable[[str], Any]]:
        if not custom_initializers is None and type_str in custom_initializers:
            return custom_initializers[type_str]
        if type_str in default_initializers:
            return default_initializers[type_str]
        return None

    def __getitem__(self, key: str) -> Any:
        return self.parameters[key]

    def __str__(self) -> str:
        return json.dumps(self.parameters, indent=4)
